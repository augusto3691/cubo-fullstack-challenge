import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import Alert from 'react-s-alert';

import { PasswordForgetLink } from '../PasswordForget';
import { withFirebase } from '../../containers/Firebase';
import * as ROUTES from '../../constants/routes';
import * as ERRORS from '../../constants/errors';

import { Form, Input, Button, Card } from 'antd';

import logo from "../../assets/logo.png";

const SignInPage = () => (
    <div className={"login-wrapper"}>
        <SignInForm />
    </div>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    isLoading: false,
};

class SignInFormBase extends Component {
    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const { email, password } = this.state;

                this.setState({
                    isLoading: true
                })

                this.props.firebase
                    .doSignInWithEmailAndPassword(email, password)
                    .then((response) => {
                        this.setState({ ...INITIAL_STATE });
                        this.props.history.push(ROUTES.DASHBOARD);
                    })
                    .catch(error => {
                        Alert.error(ERRORS.auth[error.code], {
                            position: 'bottom-right',
                            effect: 'stackslide',
                        });
                        this.setState({
                            isLoading: false
                        })
                    });
            }
        });
    };

    onChange = event => {
        if (event.target.name === "email") {
            event.target.value = event.target.value.trim();
        }
        this.setState({ [event.target.name]: event.target.value });
    };

    componentDidMount() {
        if (JSON.parse(localStorage.getItem('authUser'))) {
            this.props.history.push('/dashboard')
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="login-panel">
                <div className="text-center">
                    <img width={"200px"} className="mb-20" src={logo} alt="logo" />
                </div>
                <Form onSubmit={this.onSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Insira seu E-mail' }],
                        })(
                            <Input
                                name="email"
                                autoComplete="false"
                                className="seamless-input-transaction"
                                onChange={this.onChange}
                                placeholder="E-mail"
                                size="large"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Insira sua senha' }],
                        })(
                            <Input
                                name="password"
                                autoComplete="false"
                                className="seamless-input-transaction"
                                onChange={this.onChange}
                                type="password"
                                placeholder="Password"
                                size="large"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button
                            size="large"
                            block
                            loading={this.state.isLoading}
                            type="primary"
                            htmlType="submit"
                            shape="round"
                            className="master-sort-button">
                            Log in
                        </Button>
                    </Form.Item>
                    <Card className="master-sort-panel" bordered={false}>
                        <div className="text-center">
                            <PasswordForgetLink />
                            Não é cadastrado?  <Link to={ROUTES.SIGN_UP}>Cadastre-se aqui</Link>
                        </div>
                    </Card>

                </Form>
            </div>
        );
    }
}

const WrappedLoginForm = Form.create({ name: 'login_form' })(SignInFormBase)
const SignInForm = compose(
    withRouter,
    withFirebase,
)(WrappedLoginForm);

export default SignInPage;

export { SignInForm };