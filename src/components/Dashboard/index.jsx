import React, { Component } from 'react';
import { compose } from 'recompose';
import { withAuthorization, withEmailVerification } from "../../containers/Session";
import { withFirebase } from "../../containers/Firebase";
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import Alert from 'react-s-alert';

import { Chart, Geom, Axis, Tooltip, Coord, Legend } from 'bizcharts';

import { Button, Form, Input, Row, Col, Table, Progress, Modal } from 'antd'

import axios from 'axios'

const { confirm } = Modal;

const INITIAL_STATE = {
    isSending: false,
    participants: null
}

class Dashboard extends Component {

    constructor() {
        super()
        this.state = { ...INITIAL_STATE }
    }

    getAxiosInstance() {
        return axios.create({
            baseURL: 'https://us-central1-cubo-fullstack-challenge.cloudfunctions.net/',
            headers: {
                "Authorization": "Bearer " + (localStorage.getItem('authUser') && JSON.parse(localStorage.getItem('authUser')).jwtToken),
            },

        })
    }

    componentDidMount() {
        this.getAxiosInstance().get('participantsFetchAll').then((res) => {
            console.log(res.data);
            this.setState({
                participants: res.data
            })
        }).catch((error) => {
            this.setState({
                isSending: false
            })

            Alert.error("Could not handle the request!😓 Try again later", {
                position: 'bottom-right',
                effect: 'stackslide',
            });

        })
    }

    registerParticipant = (values) => {
        this.getAxiosInstance().post('participantsCreate', values).then((res) => {
            Alert.success("Participant added! 🎉", {
                position: 'bottom-right',
                effect: 'stackslide',
            });

            window.location.reload();
        }).catch((error) => {
            console.log(error);

            this.setState({
                isSending: false
            })

            Alert.error("Could not handle the registration!😓 Try again later", {
                position: "bottom-right",
                effect: 'stackslide',
            });

        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    isSending: true
                })

                let totalParticipation = 0;

                _.forEach(this.state.participants, (e, i) => {

                    totalParticipation += Number(e.participation)

                })

                totalParticipation += Number(values.participation);

                console.log(totalParticipation);

                let This = this;

                if (totalParticipation > 100) {
                    confirm({
                        title: 'Do you want to continue?',
                        content: 'This action will create an inconsistency considering that the total of the participations will be more than 100%. Do you want to continue?',
                        onOk() {
                            This.registerParticipant(values)
                        },
                        onCancel() {
                            This.setState({
                                isSending: false
                            })
                        },
                    });
                } else {
                    this.registerParticipant(values);
                }
            }
        });
    }

    handleValidateParticipation = (field, value, callback) => {

        if (!value || value > 100 || value < 0) {
            callback("Please input a valid 0 - 100% percentage");
        }

        callback();
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <div className="form-wrapper">
                    <div className={"container"}>
                        <Form className={"flex-space-evenly"} layout="inline" onSubmit={this.handleSubmit}>
                            <Form.Item>
                                {getFieldDecorator('firstName', {
                                    rules: [{ required: true, message: 'Please input your first name!' }],
                                })(
                                    <Input
                                        className={"baseInput"}
                                        placeholder="First Name"
                                        style={{ width: 300 }}
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('lastName', {
                                    rules: [{ required: true, message: 'Please input your last name!' }],
                                })(
                                    <Input
                                        className={"baseInput"}
                                        placeholder="Last Name"
                                        style={{ width: 300 }}
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('participation', {
                                    rules: [{ validator: this.handleValidateParticipation }],
                                })(
                                    <Input
                                        className={"baseInput"}
                                        type={"number"}
                                        placeholder="Participation"
                                        style={{ width: 300 }}
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                <Button loading={this.state.isSending} className={"send-btn"} type="primary" htmlType="submit">
                                    {this.state.isSending ? "SENDING" : "SEND"}
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
                <div className={"container"}>
                    <div className={"text-center mt-50"}>
                        <h1><strong>Data</strong></h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <Row className={"mt-50"} gutter={30}>
                        <Col md={!_.isEmpty(this.state.participants) ? 12 : 24}>
                            <Table
                                locale={{
                                    emptyText: "No data yet, you can insert some with form above!"
                                }}
                                pagination={{ pageSize: 5 }}
                                columns={
                                    [
                                        {
                                            title: 'First Name',
                                            dataIndex: 'firstName',
                                            key: 'firstName',
                                            render: text => <span>{text}</span>,
                                        },
                                        {
                                            title: 'Last Name',
                                            dataIndex: 'lastName',
                                            key: 'lastName',
                                            render: text => <span>{text}</span>,
                                        },
                                        {
                                            title: 'Participation',
                                            dataIndex: 'participation',
                                            key: 'participation',
                                            render: participation => <Progress percent={Number(participation)} size="small" />,
                                        },
                                    ]
                                }
                                dataSource={this.state.participants}>
                            </Table>
                        </Col>

                        {!_.isEmpty(this.state.participants) &&
                            <Col md={12}>
                                <Chart
                                    height={388}
                                    data={this.state.participants}
                                    forceFit
                                    padding="auto"
                                >
                                    <Coord type="theta" radius={0.70} />
                                    <Axis name="participation" />
                                    <Legend position="right-center" />
                                    <Tooltip
                                        showTitle={false}
                                        itemTpl='<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{firstName} {lastName}: {value}%</li>'
                                    />
                                    <Geom
                                        type="intervalStack"
                                        position="participation"
                                        color='fullName'
                                        tooltip={[
                                            'firstName*lastName*participation',
                                            (firstName, lastName, participation) => {
                                                return {
                                                    firstName: firstName,
                                                    lastName: lastName,
                                                    value: participation,
                                                };
                                            },
                                        ]}
                                        style={{
                                            lineWidth: 1,
                                            stroke: '#fff',
                                        }}
                                    />
                                </Chart>
                            </Col>
                        }
                    </Row>
                </div>
            </div>
        );
    }
}

const condition = authUser => !!authUser; // just check if its not null
const WrappedForm = Form.create({ name: 'registger_form' })(compose(
    withEmailVerification,
    withAuthorization(condition),
    withRouter,
    withFirebase,
)(Dashboard));

export default WrappedForm;