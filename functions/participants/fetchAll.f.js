import {
    https,
} from 'firebase-functions'
import admin from 'firebase-admin'

let cors = require("cors");

export default https.onRequest((req, res) => {
    let corsFn = cors();
    corsFn(req, res, async () => {
        let participants = [];
        const db = admin.firestore();
        await db.collection('participants').get().then(snapshot => {
            snapshot.forEach((doc) => {
                participants.push({
                    key: doc.data().key,
                    firstName: doc.data().firstName,
                    lastName: doc.data().lastName,
                    fullName: doc.data().fullName,
                    participation: Number(doc.data().participation),
                });
            })
            return;
        })
        console.log(participants);
        res.status(200).send(participants);



    })
})