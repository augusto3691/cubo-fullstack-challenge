import {
    https,
} from 'firebase-functions'
import admin from 'firebase-admin'
import _ from 'lodash'

let cors = require("cors");

export default https.onRequest((req, res) => {
    let corsFn = cors();

    corsFn(req, res, () => {
        const db = admin.firestore();
        let data = req.body;


        db.collection('participants').add({
            key: Date.now(),
            firstName: data.firstName,
            lastName: data.lastName,
            fullName: data.firstName + " " + data.lastName,
            participation: data.participation,
        }).then(ref => {
            res.status(200).send({
                doc: ref.id
            });
        }).catch(err => {
            res.status(500).send({
                err: err
            });
        })


    })
})