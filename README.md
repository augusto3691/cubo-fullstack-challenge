# Cubo Fullstack Challenge

Esse projeto contém o resultado do challenge proposto para vaga de techlead no Cubo. Além do escopo do projeto, fiz também a autenticação, validação de email e um tratamento de inconsistencia para mais de 100%.

Fiz também o deploy da aplicação para testes aqui:
https://cubo-fullstack-challenge.web.app/

Podem cirar quantos usuários quiserem, porém se quiaserem usar um usuário já cadastrado podem usar o:

    user: augusto@yopmail.com
    password: w64c74h41

O front esta dentro da pasta `/src` o back são micro-serviços como funcções lambda dentro da pasta `/functions`.

## Prerequisite :white_check_mark:

* [node](https://nodejs.org/en/) - v8 ou superior
* [yarn](https://yarnpkg.com/pt-BR/) - v1.15 ou superior

## Iniciando :zap:
    git clone https://augusto3691@bitbucket.org/augusto3691/cubo-fullstack-challenge.git
    cd cubo-fullstack-challenge
    yarn install && yarn start

## Deployment :rocket:

    yarn build
    //Os assets ficarão na pasta build

## Feito com :package:

* [create-react-app](https://github.com/facebook/create-react-app) - Skeleton Structure
* [firebase](https://www.npmjs.com/package/firebase) - NoSQL Database
* [antd](https://github.com/ant-design/ant-design/) - Styles Components e Structures

## Quem participou? :busts_in_silhouette:

* **Augusto Coelho** - *São Paulo* - [Email](mailto:augustocoelhohenriques@gmail.com.br)
